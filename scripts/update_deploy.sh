#!/bin/sh

# -- OPTIONS

# Git repository directory
SOURCE_GIT_DIR="__GIT_DIR__"

# Deployment target directories
export DEPLOY_WEB_DIR="__WWW_DIR__"


# -- FUNCTIONS

die() {
	echo "Error: $@" 1>&2
	exit 1
}


# -- PREPARATION

# Check arguments
if [ $# -gt 2 ]; then
	die "only two arguments allowed (try --help)"
fi

DO_CLEANONLY=
DO_BUILDONLY=

while [ $# -gt 0 ]; do
	case "$1" in
	--help|-h)
		echo "Usage: $0 [--clean] [--buildonly]"
		echo "Updates git repository (git pull), builds and deploys the web application."
		echo
		echo "  --buildonly  Only build (make), no deployment"
		echo "  --clean      ONLY run 'make clean' in repository (DOES NOT pull/build/deploy, ignores all other arguments)"
		exit 1
		;;
	
	--buildonly)
		DO_BUILDONLY=1
		shift
		;;
	
	--clean)
		DO_CLEANONLY=1
		shift
		;;
	
	*)
		die "unknown argument '$1' (try --help)"
		;;
	esac
done


# -- SOURCE UPDATE

# Change working directory
echo "-- Changing to git directory '$SOURCE_GIT_DIR' ..."
cd "$SOURCE_GIT_DIR" || die "could not change directory"

echo

# Alternative function --clean
if [ "$DO_CLEANONLY" ]; then
	echo "-- Running 'make clean'"
	make clean || die "make clean failed"
	
	echo
	
	echo "-- Cleaning finished."
	exit 0
fi

# Git pull
echo "-- Pulling git repository ..."
git pull || die "git pull failed"

echo


# -- BUILD THINGS

# Build web client
echo "-- Building web application ..."
make web || die "make web failed"

echo

# Done, if --buildonly specified
if [ "$DO_BUILDONLY" ]; then
	echo "-- Build finished."
	exit 0
fi


# -- DEPLOY THINGS

# Deploy web client
echo "-- Deploying web application to '$DEPLOY_WEB_DIR' ..."
make webdeploy || die "make webdeploy failed"

echo


# -- DONE :)

echo "-- Deployment finished."

