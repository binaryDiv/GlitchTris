/*
 * Game class
 */
class TetrisGame {
	constructor(_canvas) {
		// References to the canvas element and its context
		this.canvasElement = _canvas;
		this.canvasContext = _canvas.getContext("2d");
		
		// Block size in pixels
		this.blockSize = 32;
		
		// Game field size
		this.gameFieldWidth = 10;
		this.gameFieldHeight = 18;
		
		// Game field array
		this.gameField = null;
	}
	
	/*
	 * Initializes game field.
	 */
	initGame() {
		// Create game field array
		this.gameField = [];
		
		// Create row arrays to create two-dimensional array
		for (let row = 0; row < this.gameFieldHeight; row++) {
			this.gameField[row] = [];
			
			// Fill rows with initial value
			for (let col = 0; col < this.gameFieldWidth; col++) {
				// For now, use strings that name a color. TODO use numbers or something
				this.gameField[row][col] = "";
			}
		}
		
		// XXX insert some test blocks
		this.gameField[0][9] = "blue";
		for (let i = 0; i < this.gameFieldWidth; i++) {
			this.gameField[17 - i%2][i] = "red";
		}
	}
	
	/*
	 * Starts the main game loop (drawing frames etc.).
	 */
	startLoop() {
		// TODO ... window.requestAnimationFrame(callback);
	}
	
	/*
	 * Clears the canvas.
	 */
	clear() {
		this.canvasContext.clearRect(0, 0, this.canvasElement.width, this.canvasElement.height);
	}
	
	/*
	 * Clears canvas and draws one frame.
	 */
	drawFrame() {
		let ctx = this.canvasContext;
		
		// Clear canvas
		this.clear();
		
		// Set drawing styles
		ctx.fillStyle = "#FF0000";
		ctx.strokeStyle = "#000000";
		ctx.lineWidth = 2;
		
		// Draw game field
		for (let row = 0; row < this.gameFieldHeight; row++) {
			for (let col = 0; col < this.gameFieldWidth; col++) {
				// Draw block if there is one
				let color = this.gameField[row][col];
				if (color != "") {
					this.drawBlock(row, col, color);
				}
			}
		}
		
		// TEST: draw something
		this.drawBlock(5, 3, "green");
		this.drawBlock(5, 4, "green");
		this.drawBlock(5, 5, "green");
		this.drawBlock(6, 4, "green");
		
		this.drawBlock( 9, 6, "teal");
		this.drawBlock( 9, 7, "teal");
		this.drawBlock(10, 7, "teal");
		this.drawBlock(10, 8, "teal");
	}
	
	/*
	 * Draws one block.
	 */
	drawBlock(row, col, color) {
		let ctx = this.canvasContext;
		
		// Reset path and create rectangle
		ctx.beginPath();
		ctx.rect(col * this.blockSize, row * this.blockSize, this.blockSize, this.blockSize);
		
		// Set style
		ctx.fillStyle = color;
		
		// Actually draw block
		ctx.fill();
		ctx.stroke();
	}
}

// Global variable for game instance (object still needs to be created)
var tetrisGameInstance;

// Run initializations when DOM is ready
$(document).ready(function() {
	// Get canvas element
	let canvas = $("canvas#game-canvas")[0];
	
	// Error if canvas does not exist
	if (canvas === undefined) {
		console.error("Initialization failed: canvas#game-canvas does not exist!");
		return false;
	}
	
	// Initialize game instance
	tetrisGameInstance = new TetrisGame(canvas);
	tetrisGameInstance.initGame();
	
	// Draw single frame
	tetrisGameInstance.drawFrame();
	
	// Set button events
	$("button#game-button-refresh").on("click", function() {
		tetrisGameInstance.drawFrame();
	});
});
