# == Makefile ==

# Project directories
WEBDIR := web


# MAKE TARGETS
# ------------

# Default 'all' target
all: web

# Web
web:
	$(MAKE) -C $(WEBDIR)

webclean:
	$(MAKE) -C $(WEBDIR) clean

webdeploy:
	$(MAKE) -C $(WEBDIR) deploy

# Deployment
deploy: webdeploy

# Clean generated files
clean: webclean

.PHONY: all web webclean webdeploy clean deploy

